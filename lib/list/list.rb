module List
    
   
    Node= Struct.new(:value, :next)

    class List

        attr_reader :inicio
        def initialize(node)
            @inicio= node
        end
        
        def extract_beg()
            
            if (@inicio != nil)
                aux= @inicio
                @inicio= @inicio.next
                return aux
            else
                return nil
            end
        end
        
        def insert_single(nodo)
            if(@inicio == nil)
                @inicio= nodo
            else
                aux= @inicio.next
                @inicio= nodo
                @inicio.next= aux
            end
        end
        
        def insert_multiple(nodos)
            nodos.each do |element|
                insert_single(element)
            end
        end
    end

end