require 'spec_helper'
require 'list'


describe List do
  
  nodo1 = List::Node.new(2, nil)
  nodo2= List::Node.new(4, nil)
  list= List::List.new(nodo1)
  conj_nodos= []
  a= 2
  
  for i in 0..3 do
    conj_nodos[i]= List::Node.new(a, nil)
    a += 2
  end
  
  
  
  it "Existe un nodo con sus datos y su siguiente" do
    
    expect(nodo1.value).to eq(2)
    expect(nodo1.next).to eq(nil)
    
  end
  
  
  it "Existe una lsita con su cabeza" do
    
    expect(list.inicio).to eq(nodo1)
    
  end
  
  it "Se extrae el primer elemento de la lista" do
    
    expect(list.extract_beg()).to eq(nodo1)
    
  end
  
  
  it "Se puede insertar un elemento en la lista" do
    
    list.insert_single(nodo2)
    expect(list.inicio).to eq(nodo2)
    
  end
  
  it "Se pueden insertar varios elementos" do
    
    list.insert_multiple(conj_nodos)
    expect(list.inicio).to eq(conj_nodos[3])
    
  end
  
end
